# FrenchRepublicanCalendar

Library providing conversion between Gregorian calendar dates and French Republican calendar dates.

Also converts 24-hour time to decimal time.

Reference: http://en.wikipedia.org/wiki/French_Republican_Calendar

Ported from Java version by [caarmen](https://github.com/caarmen): https://github.com/caarmen/french-revolutionary-calendar

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'french_republican_calendar'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install french_republican_calendar

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/janlindblom/french_republican_calendar. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the FrenchRepublicanCalendar project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/janlindblom/french-republican-calendar/src/HEAD/CODE_OF_CONDUCT.md).
